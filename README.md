This is a Twitch profile format for Transportcraft modpack.

How to import into Twitch launcher?
1. Click on Mods > Minecraft
2. Click Create Custom Profile
3. Click on Import in Or Import a previously exported profile
4. Locate the downloaded zip file

How to export your version to this git?
1. Open your Twitch profile
2. Click on the ... next to Get More Content and choose Expor profile
3. Make sure to include folders: addons, config, resources & scripts. EXCLUDE FOLDERS: mods (any custom non-curse mods) & config/immersiverailroading
4. Unpack the created zip into the git folder