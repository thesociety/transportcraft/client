# 5.6.3.0
**Update**
- CC: Tweaked
- Custom Main Menu
- Extracells 2
- Fexcraft Common Library
- Fex's Random Stuff Mod
- Immersive Vehicles
- IV Trin Civil Pack V2
- IV Trin Part Pack
- Journeymap
- Nuclearcraft
- Tinkers Construct
- Transport Simulator Official Vehicle Set
- UNU Civilian Vehicles
- Unu Parts Pack
- Chris' Collection of German Stuff

# 5.6.2.0

**Update**
- AE2 Wireless Terminal Library
- CC: Tweaked
- ConnectedTexturesMod
- CraftTweaker
- FTB Library & FTB Utilites
- Immersive Railroading
- Immersive Vehicles
- IV Trin Civil Pack
- IV Trin Part Pack
- JourneyMap 
- Moonspire Metro & Municipal Pack for IC
- MTS Heavy Industrial Contentpack
- Nuclearcraft
- OpenComputers
- Torchmaster
- UNU Civilian Vehicles and parts packs [IV]
- Wireless Crafting Terminal

# 5.6.1.0

**Remove**
- Trains IR Union Pacific Heavy Industrial resourcepack - *Trains are oversized and laggy*

**Re-add**
- IR American Steam Locomotive Pack - *Twitch issues are solved and it wasn't the pack that people said is laggy*

**Update**
- FTB Utilites
- Immersive Vehicles Trin Civil Pack V2 (Extended version)
- Immersive Vehicles Trin Part Pack

# 5.6.0.0

**Remove**
- IR American Steam Locomotive Pack - *It has issues downloading in Twitch Launcher and I remember people saying it is too heavy in the game also*

**Update**
- **Forge to 14.23.5.2847**
- Applied Energistics 2
- BlackThorne Civilian Vehicles [MTS/IV] [IR] (cars trucks planes trains)
- Building Gadgets
- CC: Tweaked
- Charset Lib
- Chisel
- CodeChicken Lib 1.8.+
- ConnectedTexturesMod
- DiscordRichPresence
- ExtraCells2
- Forestry
- Forge MultiPart CBE
- FTB Backups
- FTB Library
- FTB Utilities
- Immersive Engineering
- Immersive Railroading
- Immersive Vehicles (Formerly Transport Simulator)
- Immersive Vehicles Trin Civil Pack V2 (Extended version)
- Immersive Vehicles Trin Part Pack
- Industrial Foregoing
- Iron Chests
- JourneyMap
- Just Enough Items (JEI)
- NuclearCraft
- p455w0rd's Library
- Shadowfacts' Forgelin
- SimpleLogic Gates
- SimpleLogic Wires
- Storage Drawers
- Torchmaster
- Track API
- Transport Simulator - Official Vehicle Set
- UNU Parts Pack [MTS]
- Wireless Redstone CBE
- Amtrak+ [Immersive Railroading Pack]
- IR American Passenger Addon Pack