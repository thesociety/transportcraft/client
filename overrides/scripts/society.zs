//Fixes MTS Custom bench recipe overlapping with another recipe
recipes.remove(<mts:custombench>);
recipes.addShaped(<mts:custombench>, [
    [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],
    [<minecraft:iron_ingot>, null, <minecraft:iron_ingot>],
    [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>],
]);